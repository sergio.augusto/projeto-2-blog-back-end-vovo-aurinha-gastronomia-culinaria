Projeto 2 - Receitas

-- Propósito
A API foi desenvolvida com o intuito de manifestar um espírito colaborativo entre os participantes dos Bootcamps.

-- O Sistema Proposto
O Sistema foi desenvolvido com o intuito de realizar todas as operações do CRUD (create, read, update, delete). Seu objetivo é manipular um arquivo JSON, realizando as operações desejadas pelo cliente e ao final retornar uma mensagem de confirmação.

-- Como baixar o projeto
Crie uma nova pasta (obs. Fora da pasta do projeto de front) -- Abra a pasta com o Visual Studio Code


-- No terminal do Visual Studio Code execute os seguintes comandos:
comando: git clone https://gitlab.com/daniel.bsilva/infnet-projeto02-api.git -- comando: cd infnet-projeto02-api


--Instalar bibliotecas
No terminal do Visual Studio Code execute o comando:
comando: npm install

-- Baixar a versão mais recente da API
Obs. Conforme vamos evoluindo no projeto certas modificações na API são necessárias. Antes de executar a API. Digite este comando no terminal do visual Studio Code, ele atualizará a API com as modificações que fiz.)
comando: git pull origin master


-- Iniciar a API
comando: npm run dev


-- Informações
Porta: 3001

-- Endereço 
http://localhost:3001


-- Campos
Os campos da especialidade e do receita são os seguintes: -- especialidade: id, descricao. -- receita: receitaId, receitaTitulo, receitaPostadaEm, receitaIdEspecialidade, receitaImagem, receitaIngredientes, receitaModoPreparo, receitaNumeroLike,receitaNumeroDislike, receitaValorAvaliacao, receitaQtdeAvaliacoes.

Rotas de especialidade
-- inserir uma especialidade - http://localhost:3001/v1/especialidade O usuário deve inserir apenas a descricao. O ID é gerado automaticamente pelo Back-End

-- consulta todas as especialidades - http://localhost:3001/v1/especialidade O usuário não precisa fornecer nenhum dado.

-- consulta uma especialidade pelo seu ID - http://localhost:3001/v1/especialidade/:id O usuário deve inserir o ID da especialidade que ele quer consultar.

-- atualiza uma especialidade - http://localhost:3001/v1/especialidade/:id O usuário deve inserir o ID da especialidade que ele quer atualizar. Dados: receitaNumeroLike, receitaNumeroDislike, receitaValorAvaliacao, receitaQtdeAvaliacoes.

-- deleta uma especialidade - http://localhost:3001/v1/especialidade/:id O usuário deve inserir o ID da especialidade que ele quer deletar.


Rotas de receita
-- inserir uma receita - http://localhost:3001/v1/receita A API precisa dos respectivos dados: receitaTitulo, receitaPostadaEm, receitaIdEspecialidade, receitaImagem, receitaIngredientes: [array de strings], receitaModoPreparo:[array de strings], receitaNumeroLike, receitaNumeroDislike, receitaValorAvaliacao, receitaQtdeAvaliacoes.

-- consultar todas as receitas - http://localhost:3001/v1/receita O usuário não precisa fornecer nenhum dado.

-- consultar uma receita pelo seu ID - http://localhost:3001/v1/receita/:receitaId O usuário deve inserir o ID da receita que ele quer consultar.

-- lista receitas por especialidade - http://localhost:3001/v1/especialidade/:id/receita O usuário deve inserir o ID da especialidade. O Sistema listará as receitas pertencentes aquela especialidade.

-- atualizar uma receita - http://localhost:3001/v1/receita/:receitaId O usuário deve inserir o ID da receita que ele quer atualizar. Não serão todos os dados, apenas: receitaNumeroLike, receitaNumeroDislike, receitaValorAvaliacao, receitaQtdeAvaliacoes.

-- deletar uma receita - http://localhost:3001/v1/receita/:receitaId O usuário deve inserir o ID da receita que ele quer excluir.

Obs. Para facilitar no front, a API retorna além do 'message' também o 'error' true caso ele ocorra. Desse modo você consegue ver se ocorreu um erro ou não. E também terá a menssagem. Podendo validar e exibir no seu front. Em caso de sucesso ele retorna uma 'message' confirmando.

Abraço Daniel Barboza.

Alteração feita em uma imagem do JSON de receitas.

Sérgio Loureiro Augusto.