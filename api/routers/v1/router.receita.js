const receitaController = require('../../controllers/controllers.receita');

module.exports = (router) => {
  router.route('/receita').post(receitaController.criaReceita);
  router.route('/receita').get(receitaController.listarReceitas);
  router.route('/receita/:id').get(receitaController.listarReceitaPorId);
  router.route('/receita/:id').put(receitaController.updateReceita);
  router.route('/receita/:id').delete(receitaController.excluirReceitaPorId);

};
