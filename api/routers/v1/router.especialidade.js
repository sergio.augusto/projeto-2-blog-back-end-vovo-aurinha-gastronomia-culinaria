const especialidadeController = require('../../controllers/controllers.especialidade');

module.exports = (router) => {
    router.route('/especialidade').post(especialidadeController.criarEspecialidade);
    router.route('/especialidade').get(especialidadeController.listarEspecialidades);
    router.route('/especialidade/:id').get(especialidadeController.listarEspecialidadePorId);
    router.route('/especialidade/:id').put(especialidadeController.updateEspecialidade);
    router.route('/especialidade/:id').delete(especialidadeController.excluirEspecialidadePorId);
    router.route('/especialidade/:id/receita').get(especialidadeController.listarReceitaPorEspecialidade);

}


