const { Router } = require('express');
const { name, version } = require('../../package.json');

const routesV1Especialidade = require('./v1/router.especialidade');
const routesV1Receita = require('./v1/router.receita');

module.exports = (app) => {

    app.get('/', (req, res, next) => {
        res.send({ name, version });
    });

    const routesV1 = Router();

    routesV1Especialidade(routesV1);
    routesV1Receita(routesV1);

    app.use('/v1', routesV1);

}

