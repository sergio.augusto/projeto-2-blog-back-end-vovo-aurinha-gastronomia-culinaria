const uuid = require('uuid').v4;
const { saveFile, openFile } = require('../utils/utils');
const receitasDB = 'receitas';

// Listar todas as receitas
const listarReceitas = async (req, res, next) => {
  var receitas = await openFile(receitasDB);
  res
    .status(200)
    .send({ receitas, message: `As receitas foram listadas com sucesso` });
};

// Listar a receita pelo ID
const listarReceitaPorId = async (req, res, next) => {
  const receitaId = req.params.id;
  var receitas = await openFile(receitasDB);

  const result = await receitas.find((cat) => cat.receitaId == receitaId);

  if (!result) {
    res.status(409).send({
      error: true,
      message: 'A receita não foi encontrada.'
    });
  } else {
    res.status(200).send({
      result,
      message: `A receita com id: ${receitaId} foi listada com sucesso`
    });
  }
};

const listarReceitaPorEspecialidade = async (req, res, next) => {
  const id = req.params.id;

  var receitas = await openFile(receitasDB);

  const listaReceitas = receitas.filter(
    (item) => item.receitaIdEspecialidade == id
  );
  const verificaReceita = receitas.find(
    (item) => item.receitaIdEspecialidade == id
  );

  if (!verificaReceita) {
    res.status(409).send({
      error: true,
      message: 'Não existe receitas com o ID informado'
    });
    return -1;
  }

  res
    .status(200)
    .send({ listaReceitas, message: 'Receitas(s) listadas com sucesso' });
  return 0;
};

// Inserir no BD uma receita
const criaReceita = async (req, res, next) => {
  var receitas = await openFile(receitasDB);
  const body = req.body;

  const result = {
    receitaId: uuid(),
    receitaTitulo: body.receitaTitulo,
    receitaPostadaEm: body.receitaPostadaEm,
    receitaIdEspecialidade: body.receitaIdEspecialidade,
    receitaImagem: body.receitaImagem,
    receitaIngredientes: body.receitaIngredientes,
    receitaModoPreparo: body.receitaModoPreparo,
    receitaNumeroLike: body.receitaNumeroLike,
    receitaNumeroDislike: body.receitaNumeroDislike,
    receitaValorAvaliacao: body.receitaValorAvaliacao,
    receitaQtdeAvaliacoes: body.receitaQtdeAvaliacoes
  };

  if (
    !body.receitaTitulo ||
    !body.receitaPostadaEm ||
    !body.receitaIdEspecialidade ||
    !body.receitaImagem ||
    !body.receitaIngredientes ||
    !body.receitaModoPreparo ||
    !body.receitaNumeroLike ||
    !body.receitaNumeroDislike ||
    !body.receitaValorAvaliacao ||
    !body.receitaQtdeAvaliacoes
  ) {
    res.status(400).send({ message: `Você deve preencher todos os campos` });
    return -1;
  }

  const verificaReceita = await receitas.find(
    (v) => v.receitaTitulo == body.receitaTitulo
  );

  if (verificaReceita) {
    // Verifica se o receita já existe na base de dados
    res.status(409).send({
      error: true,
      message: `Já existe uma receita com esse título na base de dados`
    });
  } else {
    // Sucesso, insere os dados na base
    await receitas.push(result);
    await saveFile(receitasDB, receitas);
    res.status(200).send({ message: `A receita foi criada com sucesso.` });
  }
};

// Excluir no BD uma receita
const excluirReceitaPorId = async (req, res, next) => {
  var receitas = await openFile(receitasDB);
  const id = req.params.id;
  const result = receitas.findIndex((cat) => cat.receitaId == id);
  const verificar = receitas.find((cat) => cat.receitaId == id);

  if (!verificar) {
    res.status(409).send({
      error: true,
      message: 'id não encontrado.'
    });
  } else {
    // index do array que eu quero mudar e a qtd de objetos
    const resp = await receitas.splice(result, 1);
    await saveFile(receitasDB, receitas);
    res.status(200).send({
      message: `A receita com id: ${id} foi excluida com sucesso`
    });
  }
};

// Atualizar no BD uma receita
const updateReceita = async (req, res, next) => {
  const receitas = await openFile(receitasDB);
  const id = req.params.id;
  if (id == 0) {
    res.status(409).send({ message: 'ID não encontrado!' });
    return -1;
  }
  const resultIndex = receitas.findIndex((cat) => cat.receitaId == id);
  const consulta = receitas.find((cat) => cat.receitaId == id);

  const body = req.body;

  const result = {
    receitaId: consulta.receitaId,
    receitaTitulo: consulta.receitaTitulo,
    receitaPostadaEm: consulta.receitaPostadaEm,
    receitaIdEspecialidade: consulta.receitaIdEspecialidade,
    receitaImagem: consulta.receitaImagem,
    receitaIngredientes: consulta.receitaIngredientes,
    receitaModoPreparo: consulta.receitaModoPreparo,
    receitaNumeroLike: body.receitaNumeroLike,
    receitaNumeroDislike: body.receitaNumeroDislike,
    receitaValorAvaliacao: body.receitaValorAvaliacao,
    receitaQtdeAvaliacoes: body.receitaQtdeAvaliacoes
  };

  await receitas.splice(resultIndex, 1);
  await saveFile(receitasDB, receitas);

  await receitas.push(result);
  await saveFile(receitasDB, receitas);
  res.status(200).send({
    message: `A receita foi atualizada com sucesso.`
  });
};

module.exports = {
  criaReceita,
  listarReceitas,
  listarReceitaPorId,
  excluirReceitaPorId,
  updateReceita,
  listarReceitaPorEspecialidade
};
