const uuid = require('uuid').v4;
const { saveFile, openFile } = require('../utils/utils');
const especialidadesDB = 'especialidades';
const receitasDB = 'receitas';

// Listar todas as especialidades
const listarEspecialidades = async (req, res, next) => {
  var especialidades = await openFile(especialidadesDB);

  if (!especialidades) {
    res.status(409).send({ error: true, message: `Nenhum resultado!.` });
    return -1;
  }
  res.status(200).send({
    especialidades,
    message: `As especialidades foram listadas com sucesso`
  });
};

// Listar a especialidade pelo ID
const listarEspecialidadePorId = async (req, res, next) => {
  var especialidades = await openFile(especialidadesDB);

  const id = req.params.id;
  const result = especialidades.find((cat) => cat.id == id);

  if (!result) {
    res.status(409).send({
      error: true,
      message: 'Especialidade não encontrada.'
    });
    return -1;
  } else {
    res.status(200).send({
      result,
      message: `A especialidade com id: ${id} foi listada com sucesso`
    });
  }
};

// Inserir no BD um especialidade
const criarEspecialidade = async (req, res, next) => {
  var especialidades = await openFile(especialidadesDB);

  const body = req.body;

  const result = {
    id: Math.floor(Math.random() * 11110),
    descricao: body.descricao
  };
  if (!body.descricao) {
    res.status(409).send({ message: `Preencha todos os campos!` });
    return -1;
  }
  const verifica = await especialidades.find(
    (v) => v.descricao == body.descricao
  );
  if (verifica) {
    res.status(409).send({
      error: true,
      message: `Já existe uma especialidade com essa descrição`
    });
    return -1;
  } else {
    // Sucesso, insere os dados na base
    await especialidades.push(result);
    await saveFile(especialidadesDB, especialidades);
    res
      .status(200)
      .send({ message: `A especialidade foi adicionada com sucesso.` });
  }
};

// Excluir no BD uma especialidade
const excluirEspecialidadePorId = async (req, res, next) => {
  var especialidades = await openFile(especialidadesDB);
  var receitas = await openFile(receitasDB);

  const id = req.params.id;
  const result = especialidades.findIndex((cat) => cat.id == id);
  const verificar = especialidades.find((cat) => cat.id == id);
  const verificarEspecialidade = receitas.find(
    (cat) => cat.receitaIdEspecialidade == id
  );

  if (!verificar) {
    res.status(409).send({
      error: true,
      message: 'id não encontrado.'
    });
    return -1;
  }
  if (verificarEspecialidade) {
    res.status(409).send({
      error: true,
      message: 'Especialidade não foi excluída, pois existem receita(s) vinculadas a ela !'
    });
    return -1;
  }
  // index do array que eu quero mudar e a qtd de objetos
  const resp = await especialidades.splice(result, 1);
  await saveFile(especialidadesDB, especialidades);
  res.status(200).send({
    message: `A especialidade com id: ${id} foi excluida com sucesso`
  });
  
};

// Atualizar no BD uma especialidade
const updateEspecialidade = async (req, res, next) => {
  var especialidades = await openFile(especialidadesDB);

  const id = req.params.id;

  const resultIndex = especialidades.findIndex((cat) => cat.id == id);
  const consulta = especialidades.find((cat) => cat.id == id);

  const body = req.body;

  const result = {
    id: consulta.id,
    descricao: body.descricao
  };

  if (!body.descricao) {
    res
      .status(400)
      .send({ error: true, message: `Você deve preencher todos os campos` });
    return -1;
  }

  const verificar = await especialidades.find(
    (v) => v.descricao == body.descricao
  );

  if (verificar) {
    res.status(409).send({
      error: true,
      message: `Já existe uma especialidade com essa descrição`
    });
    return -1;
  }
  if (!especialidades) {
    res
      .status(500)
      .send({ error: true, message: `Ocorreu um erro no servidor.` });
    return -1;
  } else {
    // Sucesso, insere os dados na base
    await especialidades.splice(resultIndex, 1);
    await saveFile(especialidadesDB, especialidades);

    await especialidades.push(result);
    await saveFile(especialidadesDB, especialidades);
    res.status(200).send({
      message: `A especialidade foi atualizada com sucesso.`
    });
  }
};
const listarReceitaPorEspecialidade = async (req, res, next) => {
  const id = req.params.id;

  var receitas = await openFile(receitasDB);

  const consulta = receitas.filter((cat) => cat.receitaIdEspecialidade === id);

  if (consulta?.length == []) {
    res.status(404).send({
      error: true,
      message: `Nenhum receita encontrada`
    });
    return -1;
  } else {
    res.status(200).send({
      consulta,
      message: `As receitas da especialidade: ${id} foram listadas com sucesso`
    });
    return 0;
  }
};

module.exports = {
  listarEspecialidades,
  listarEspecialidadePorId,
  criarEspecialidade,
  excluirEspecialidadePorId,
  updateEspecialidade,
  listarReceitaPorEspecialidade
};
