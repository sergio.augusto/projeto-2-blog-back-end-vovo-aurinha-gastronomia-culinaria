const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const receita = {
  receitaTitulo: { type: String, require: true },
  receitaPostadaEm: { type: String, require: true },
  receitaIdEspecialidade: [{type: Schema.Types.ObjectId, ref: 'especialidade'}],
  receitaImagem: { type: String, require: true },
  receitaIngredientes: [{ type: String }],
  receitaModoPreparo: [{ type: String}],
  receitaNumeroLike: { type: String, require: true },
  receitaNumeroDislike: { type: String, require: true },
  receitaValorAvaliacao: { type: String, require: true },
  receitaQtdeAvaliacoes: { type: String, require: true }

};
module.exports = receita;
