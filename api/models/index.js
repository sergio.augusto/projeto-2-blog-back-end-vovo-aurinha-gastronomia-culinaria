const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/* função que recebe um esquema pai e filho e objeto de opção, 
retorna uma nova instancia apartir da classe esquema */
const createSchema = (modelPai, model, options = {}) => {
  /* cria  aligação com a estrutura que criamos e o banco de dados 
atravérs da ORM 'mongoose' */
  return new Schema(
    {
      ...modelPai,
      ...model
    },
    {
      timestamps: true,
      collection: 'EspecialidadeCollection',
      ...options
    }
  );
};

const especialidadeSchema = require('./models.especialidade');
const especialidade = mongoose.model(
  'especialidade',
  createSchema(undefined, especialidadeSchema, {})
);

const receitaSchema = require('./models.receita');
const receita = mongoose.model('receita', createSchema(undefined, receitaSchema, {
  collection: 'ReceitaCollection',
  toJSON: {
    virtuals: true,
  }
}));


module.exports = {
  especialidade,
  receita
};
