const { saveFile, openFile } = require('./api/utils/utils');

const especialidadesDB = 'especialidades';
const receitasDB = 'receitas';

const adicionarDados = async (req, res, next) => {
  var especialidade = await openFile(especialidadesDB);
  var receita = await openFile(receitasDB);

  const resultReceitas = [
    {
      receitaId: '1',
      receitaTitulo: 'Melhor Torta de Chocolate do Mundo',
      receitaPostadaEm: '15/05/2021',
      receitaIdEspecialidade: '1',
      receitaImagem:
        'https://www.petiscos.com/wp-content/uploads/2020/05/77045f67817c0b7951ed0dff27693408b5d61_omelhorbolo.jpg',
      receitaIngredientes: [
        'Para o suspiro:',
        '250g de açúcar de confeiteiro',
        '250g de açúcar',
        '40g de cacau em pó',
        '8 claras',
        'Para a musse:',
        '500g de chocolate meio amargo',
        '450g de creme de leite fresco',
        '80g de açúcar',
        '4 claras',
        ''
      ],
      receitaModoPreparo: [
        'FAÇA O SUSPIRO!',
        'Antes de preparar o melhor bolo de chocolate do mundo, é preciso ter em mãos papel manteiga e, se possível, um compasso. Você também precisará de duas formas retangulares grandes.',
        'Corte um pedaço do papel manteiga na medida de uma das formas, pois ele vai servir para forrá-la. Depois, repita o procedimento, cortando outra folha com o mesmo tamanho. Caso prefira utilizar folhas avulsas desse papel, é importante que tenham, no mínimo, 60 cm de comprimento.',
        'Em uma das folhas já em formato retangular, desenhe dois círculos, com 20 cm de diâmetro cada. No outro pedaço de papel manteiga, é necessário desenhar apenas um círculo.',
        'Forre cada uma das formas com o papel já desenhado. Ele deve ocupar toda a extensão da assadeira retangular e os círculos desenhados precisam ter espaço entre si, pois servirão como molde para a massa de suspiro do seu bolo.',
        'MODO DE PREPARO:',
        'Peneire o açúcar de confeiteiro e o cacau em pó em uma tigela. Reserve.',
        'Em seguida, bata as claras em neve. Para isso, mantenha a velocidade baixa, até a mistura começar a espumar. Junte um pouco do açúcar comum e continue a bater.',
        'Quando o conteúdo formar picos moles, adicione o restante do açúcar aos poucos, sem parar de bater. Desligue a batedeira.',
        'Feito isso, adicione o açúcar de confeiteiro e cacau em pó que você já peneirou às claras em neve e misture tudo com uma colher, delicadamente.',
        'Com a ajuda de um saco de confeitar, preencha os círculos desenhados no papel, com a massa que obteve. Para isso, faça um caracol em cada círculo.',
        'Leve as assadeiras ao forno para assar a 120ºC (temperatura baixa) por 1 hora.',
        'Preste atenção à temperatura do forno, que deve se manter baixa. Quando o suspiro estiver pronto, retire as assadeiras do forno e deixe arrefecer',
        'FAÇA A MUSSE DE CHOCOLATE!',
        'MODO DE PREPARO:',
        'Comece picando todo o chocolate ao leite e, depois, derreta em banho-maria, ou no microondas. Reserve. Bata o creme de leite fresco na batedeira, até que vire uma espécie de chantilly. Coloque em uma vasilha e reserve.',
        'Mais uma vez, bata as claras em neve e, depois, junte-as ao chocolate derretido e misture delicadamente com uma colher. O próximo passo é despejar o creme de leite batido na mistura e voltar a mexer com a colher.',
        'Monte o melhor bolo do mundo!',
        'Acerte os discos de suspiro com uma faca, para que eles fiquem simétricos. Em seguida, coloque um dos discos sobre o recipiente final, em que servirá a sobremesa, e despeje parte do mousse de chocolate.',
        'Ponha outro disco de suspiro sobre o mousse e repita o processo de recheio. Coloque o terceiro disco sobre o mousse.',
        'Para finalizar, cubra toda a superfície do bolo e as suas laterais com o creme restante. É importante trabalhar rapidamente na montagem, para que o mousse não endureça, tornando-se difícil de manusear. Leve ao freezer por 30 minutos e sirva.',
        'Dica: Caso prepare a sobremesa para o dia seguinte, deixe-a no freezer e retire apenas uma hora antes de servir...',
        ''
      ],
      receitaNumeroLike: '35',
      receitaNumeroDislike: '3',
      receitaValorAvaliacao: '4',
      receitaQtdeAvaliacoes: '274'
    },
    {
      receitaId: '2',
      receitaTitulo: 'Tiramissú',
      receitaPostadaEm: '15/05/2020',
      receitaIdEspecialidade: '1',
      receitaImagem:
        'https://img.itdg.com.br/tdg/images/recipes/000/086/691/332009/332009_original.jpg?mode=crop&width=710&height=400',
      receitaIngredientes: [
        '1 pacote de biscoitos champangne',
        '500 g de queijo mascarpone (pode substituir com 500 g de requeijão cremoso tipo catupiry)',
        '250 ml de creme de leite fresco',
        '3 gemas',
        '150 g de açúcar',
        '1 cálice de conhaque (opcional)',
        '2 xícaras de café frio',
        'chocolate em pó',
        ''
      ],
      receitaModoPreparo: [
        'Bata o creme de leite na batedeira até obter um chantilly firme.',
        'Reserve.',
        'Bata o açúcar com as gemas e o conhaque por 3 minutos.',
        'Acrescente o chantilly, o mascarpone e misture bem.',
        'Montagem',
        'Molhe rapidamente os biscoitos no café (um a um) e coloque em um refratário formando uma camada, coloque por cima uma camada do creme, faça mais uma camada de biscoitos molhados no café e por último mais uma camada de creme.',
        'Para finalizar polvilhe chocolate em pó amargo.',
        'Leve à geladeira por no mínimo 3 horas e sirva.',
        'INFORMAÇÕES ADICIONAIS',
        'O tiramisu é uma sobremesa de origem ialiana que se popularizou no Brasil. Aqui ela sofreu algumas modificações, por isso podemos encontrar o prato sendo preparado de várias maneiras. Tiramisu em italiano quer dizer levanta-me, ou me coloque para cima.',
        'O tiramisu é feito com café, um ingrediente maravilhoso que pode ser usado tanto em pratos doces como salgados. E aqui no TudoGostoso você encontra alguns desses exemplos como o frapê de café e o docinho de café.',
        'Essa sobremesa tem o modo de preparo similar ao de um pavê! Então, que tal se arriscar na cozinha e descobrir mais delícias como essas? Veja e faça o pavê de chocolate, o pavê de morango e o pavê casadinho.',
        ''
      ],
      receitaNumeroLike: '25',
      receitaNumeroDislike: '2',
      receitaValorAvaliacao: '3',
      receitaQtdeAvaliacoes: '158'
    },
    {
      receitaId: '3',
      receitaTitulo: 'Torta Alemã',
      receitaPostadaEm: '15/05/2019',
      receitaIdEspecialidade: '1',
      receitaImagem:
        'https://img.itdg.com.br/tdg/images/recipes/000/001/412/327924/327924_original.jpg?mode=crop&width=710&height=400',
      receitaIngredientes: [
        '200 gramas de manteiga sem sal',
        '1 xícara de chá de açúcar',
        '1 lata de creme de leite',
        '1 pacote de bolacha maisena',
        'Leite o quanto baste para molhar a bolacha',
        '1 barra de chocolate ao leite',
        'Meia lata de creme de leite',
        ''
      ],
      receitaModoPreparo: [
        'Coloque a manteiga e o açúcar na batedeira e bata até obter um creme bem fofo e liso.',
        'Acrescente o creme de leite e bata rapidamente apenas para misturar.',
        'Desligue a batedeira e reserve.',
        'Separe um recipiente médio para montar o doce.',
        'Acrescente um pouco de leite num prato fundo e molhe rapidamente algumas bolachas maisena no leite.',
        'Forre o fundo do recipiente escolhido com uma camada de bolachas.',
        'Acrescente uma camada do creme reservado sobre as bolachas.',
        'Acrescente mais uma camada de bolachas molhadas no leite e repita o procedimento finalizando com a bolacha',
        'Derreta a barra de chocolate ao leite em banho-maria ou no micro-ondas e adicione o creme de leite.',
        'Cubra a última camada de bolacha com a cobertura.',
        'Leve à geladeira por no mínimo 3 horas ou até que o doce fique bem gelado.',
        'Retire o doce da geladeira e sirva a seguir.',
        ''
      ],
      receitaNumeroLike: '95',
      receitaNumeroDislike: '4',
      receitaValorAvaliacao: '2',
      receitaQtdeAvaliacoes: '359'
    },
    {
      receitaId: '4',
      receitaTitulo: 'Torta Brownie',
      receitaPostadaEm: '15/05/2021',
      receitaIdEspecialidade: '1',
      receitaImagem:
        'https://www.altoastral.com.br/media/_versions/legacy/2017/03/torta-brownie-avela_widexl.jpg',
      receitaIngredientes: [
        '1/2 xícara (chá) de avelã sem pele triturada',
        '1 e 1/2 xícara (chá) de farinha de trigo',
        '6 colheres (sopa) de manteiga amolecida',
        '6 colheres (sopa) de açúcar',
        '1 xícara (chá) de creme de avelã',
        '1 caixa de creme de leite (200g)',
        'Avelãs picadas para decorar',
        'Recheio : ',
        '6 colheres (sopa) de manteiga',
        '170g de chocolate meio amargo picado',
        '2 ovos',
        '1 xícara (chá) de açúcar',
        '1/2 xícara (chá) de farinha de trigo',
        ''
      ],
      receitaModoPreparo: [
        'Em uma tigela, coloque a avelã, a farinha, a manteiga, o açúcar e misture até ficar homogêneo.',
        'Forre o fundo de uma fôrma de aro removível média com a massa e leve ao forno médio, preaquecido, por 20 minutos ou até firmar.',
        'Retire do forno e deixe esfriar.',
        'Para o recheio, derreta a manteiga com o chocolate, em banho-maria, e reserve.',
        'Em uma tigela, com um batedor de arame, bata os ovos e o açúcar até formar uma espuma.',
        'Junte ao chocolate derretido e adicione a farinha.',
        'Misture com o batedor de arame até homogeneizar.',
        'Despeje sobre a massa e leve ao forno médio, preaquecido, por 25 minutos ou até firmar.',
        'Retire do forno, deixe esfriar e desenforme.',
        'Cubra com o creme de avelã e o creme de leite misturados.',
        'Decore com avelãs picadas e sirva.',
        ''
      ],
      receitaNumeroLike: '476',
      receitaNumeroDislike: '3',
      receitaValorAvaliacao: '5',
      receitaQtdeAvaliacoes: '637'
    },
    {
      receitaId: '5',
      receitaTitulo: 'Bacalhau à Gomes de Sá',
      receitaPostadaEm: '15/10/2019',
      receitaIdEspecialidade: '2',
      receitaImagem: 'https://www.cultuga.com.br/wp-content/uploads/2020/06/receita-bacalhau-gomesdesa-cultuga-capa.jpg',
      receitaIngredientes: [
        '500 g de bacalhau',
        '500 g de batatas',
        '2 cebolas',
        '1 dente de alho',
        '1 folha de louro',
        '2 ovos cozidos',
        '1/5 de azeite',
        'Azeitonas pretas',
        'Salsa',
        'Sal',
        'Pimenta do reino',
        ''
      ],
      receitaModoPreparo: [
        'Demolhe o bacalhau, coloque-o num tacho e escalde-o com água a ferver.',
        'Tape e abafe o recipiente com um cobertor e deixe ficar assim durante 20 minutos.',
        'Escorra o bacalhau, retire-lhe as peles e as espinhas e desfaça-o em lascas.',
        'Ponha estas num recipiente fundo, cubra-as com leite bem quente e deixe ficar de infusão durante 1 hora e meia a 3 horas.',
        'Entretanto, corte as cebolas e o dente de alho ás rodelas e leve a alourar ligeiramente com um pouco de azeite.',
        'Junte as batatas, que foram cozidas com a pele, e depois peladas e cortadas às rodelas.',
        'Junte o bacalhau escorrido. Mexa tudo ligeiramente, mas sem deixar refogar.',
        'Tempere com sal e pimenta. Deite imediatamente num tabuleiro de barro e leve ao forno bem quente durante 10 minutos.',
        'Sirva no prato em que foi ao forno, polvilhado com salsa picada e enfeitado com rodelas de ovo cozido e azeitonas pretas.',
        'Nota: Esta é a verdadeira receita de bacalhau à gomes de sá , tal como a criou o seu inventor, que foi comerciante de bacalhau no porto.',
        'Aconselho a que prepare de véspera a operação de infusão do bacalhau em leite quente, para que se torne mais rápida a preparação deste prato.',
        ''
      ],
      receitaNumeroLike: '785',
      receitaNumeroDislike: '14',
      receitaValorAvaliacao: '5',
      receitaQtdeAvaliacoes: '1.357'
    },
    {
      receitaId: '6',
      receitaTitulo: 'Costelinha de Porco com Molho de Páprica e Cravo',
      receitaPostadaEm: '15/10/2020',
      receitaIdEspecialidade: '3',
      receitaImagem:
        'https://www.kitano.com.br/wp-content/uploads/2019/08/SSP_2930-Natal-costelinhas-assadas-ao-molho-de-pa%E2%95%A0%C3%BCprica-e-cravo.jpg',
      receitaIngredientes: [
        '1 peça de costelinha de porco inteira (1,2 kg)',
        '1 colher (chá) de sal',
        '2 dentes de alho amassados',
        '1 colher (chá) de Pimenta-do-Reino Preta Moída',
        '1 colher (sopa) de Páprica Doce',
        '1 colher (sopa) de mel',
        '2 colheres (chá) de Cravo-da-Índia',
        'Meia xícara (chá) de suco de laranja',
        'Para cobrir : ',
        'Papel-alumínio',
        ''
      ],
      receitaModoPreparo: [
        'Pré-aqueça o forno em temperatura média (180ºC).',
        'Arrume as costelinhas em uma assadeira. Reserve.',
        'Em uma tigela, misture o sal, o alho, a Pimenta-do-Reino Preta Moída e a Páprica Doce, o mel e os Cravos-da-Índia.',
        'Espalhe a mistura sobre a costelinha, regue com suco e cubra com papel-alumínio.',
        'Leve ao forno por 1 hora. Retire o papel-alumínio e asse por mais 30 minutos ou até ficar macia e o molho encorpar, regando com o molho da assadeira de vez em quando.',
        'Sirva em seguida.',
        'DICAS',
        'Sirva com batatas refogadas com Alecrim.',
        ''
      ],
      receitaNumeroLike: '389',
      receitaNumeroDislike: '34',
      receitaValorAvaliacao: '4',
      receitaQtdeAvaliacoes: '157'
    },
    {
      receitaId: '7',
      receitaTitulo: 'Feijoada',
      receitaPostadaEm: '16/08/2020',
      receitaIdEspecialidade: '4',
      receitaImagem:
        'https://acarnequeomundoprefere.com.br/uploads/media/image/frimesa-receitas-eisbein-1.jpg',
      receitaIngredientes: [
        '1,1 kg de costela de porco salgada (1 peça com 12 ripas)',
        '800 g de carne-seca',
        '600 g de lombo de porco salgado',
        '1 kg de feijão-preto',
        '500 g de paio (4 unidades)',
        '3 cebolas',
        '5 dentes de alho',
        '¼ de xícara (chá) de azeite',
        '3 folhas de louro',
        '1 colher (chá) de cominho em pó',
        ''
      ],
      receitaModoPreparo: [
        'Dessalgar as carnes no dia anterior trocando a água a cada 3 horas, de preferência.',
        'Corte o lombo e a carne-seca em cubos médios de cerca de 3 cm.',
        'Corte a costelinha entre os ossinhos para separar as ripas.',
        'Coloque cada tipo de carne numa tigela grande, cubra com bastante água e leve à geladeira.',
        'Lave bem o feijão sob água corrente.',
        'Transfira os grãos para uma tigela grande e cubra com 2,5 litros de água.',
        'Cubra com um prato e deixe de molho por 30 minutos – se o feijão ficar muito tempo de molho pode perder a cor e desmanchar durante o longo cozimento da feijoada.',
        'Enquanto isso, faça o pré-cozimento das carnes.',
        'Escorra a água das carnes dessalgadas e transfira para um caldeirão grande com capacidade para 11 litros.',
        'Cubra as carnes com água e leve ao fogo alto, a quantidade pode variar de acordo com o tamanho da panela, o importante é que as carnes devem ficar completamente imersas.',
        'Deixe cozinhar por 10 minutos em fogo alto, contados após a fervura para eliminar o excesso de gordura e sal das carnes.',
        'Enquanto isso, descasque e pique fino as cebolas e os dentes de alho.',
        'Com uma faca pequena, retire a pele dos paios.',
        'Escorra a água do feijão.',
        'Passados os 10 minutos, com uma escumadeira, transfira as carnes pré-cozidas para uma tigela grande e descarte a água do cozimento, com cuidado para não se queimar.',
        'Volte o caldeirão ao fogo alto.',
        'Quando aquecer, regue com o azeite, adicione a cebola e tempere com uma pitada de sal.',
        'Refogue por cerca de 8 minutos até começar a dourar.',
        'Junte o alho, o cominho, as folhas de louro e mexa por 1 minuto para perfumar.',
        'Acrescente o feijão demolhado ao refogado e misture bem.',
        'Adicione 6 litros de água e mantenha a panela em fogo alto.',
        'Assim que começar a ferver, junte a carne-seca e a costelinha pré-cozidas, abaixe o fogo e deixe cozinhar por 2 horas, mexendo de vez em quando.',
        'Após as primeiras 2 horas de cozimento, junte o lombo pré-cozido e os paios inteiros.',
        'Deixe cozinhar em fogo baixo por mais 3 horas, mexendo de vez em quando delicadamente para não desmanchar os grãos de feijão.',
        'Faltando 30 minutos para o fim do cozimento, com uma pinça, transfira os paios para a tábua e corte cada um em fatias de 1 cm na diagonal.',
        'Volte as fatias para a panela e deixe cozinhar até completar as 5 horas de cozimento total, ou até que as carnes estejam bem macias e o caldo da feijoada comece a engrossar.',
        'Sirva com arroz, couve refogada, gomos de laranja e farinha de mandioca torrada.',
        ''
      ],
      receitaNumeroLike: '1.452',
      receitaNumeroDislike: '34',
      receitaValorAvaliacao: '5',
      receitaQtdeAvaliacoes: '3.157'
    },
    {
      receitaId: '8',
      receitaTitulo: 'Espaguete com Camarões ao Molho Rosé',
      receitaPostadaEm: '05/09/2019',
      receitaIdEspecialidade: '5',
      receitaImagem:
        'https://cdn.panelinha.com.br/receita/1530799250923-macarra%CC%83o.jpg',
      receitaIngredientes: [
        '300 g de macarrão cabelinho de anjo (10 ninhos)',
        '300 g de camarões tipo cinza médio limpos (24 unidades)',
        '1 lata de tomate pelado (com o líquido)',
        '1 dente de alho descascado',
        '¼ de xícara (chá) de creme de leite fresco',
        '3 ½ xícaras (chá) de água fervente',
        '2 colheres (sopa) de conhaque',
        'azeite a gosto',
        'sal e pimenta-do-reino moída na hora a gosto',
        'folhas de endro (dill) a gosto para servir',
        ''
      ],
      receitaModoPreparo: [
        'Leve uma chaleira com 1 litro de água ao fogo alto para ferver. Enquanto isso, no liquidificador, bata o tomate pelado (com o líquido) e o alho até ficar liso, reserve.',
        'Tempere os camarões com sal e pimenta a gosto. Leve uma frigideira grande com borda alta ao fogo médio (se preferir utilize uma panela de fundo grande). Quando aquecer, regue com 2 colheres (sopa) de azeite, adicione os camarões e deixe dourar, por 1 minuto de cada lado – é jogo rápido, se cozinharem demais os camarões podem ficar duros. Transfira para um prato e reserve.',
        'Mantenha a frigideira em fogo médio e adicione o tomate batido com alho. Tempere com sal e mexa, raspando o fundo da frigideira para dissolver os queimadinhos – eles são essenciais para dar sabor ao molho. Assim que o molho ferver, junte o conhaque, meça 3 ½ xícaras (chá) da água fervente e regue sobre o molho. Tempere com sal e misture bem.',
        'Coloque os ninhos de macarrão no molho, um ao lado do outro – assim eles cozinham de maneira uniforme. Abaixe o fogo, tampe e deixe cozinhar por cerca de 2 minutos ou até o macarrão começar a amolecer. Abra a panela e vire os ninhos com um garfo (ou pinça). Tampe novamente e deixe por mais 3 minutos ou até os fios começarem a soltar dos ninhos.',
        'Abra a tampa e solte os fios dos ninhos com um garfo. Misture o creme de leite, junte os camarões e tampe a panela. Deixe em fogo baixo por mais 1 minuto para o macarrão terminar de cozinhar e os camarões aquecerem.',
        'Desligue o fogo e, com a tesoura, corte as folhas de endro sobre o macarrão. Sirva a seguir. Fica uma delícia servido com brócolis chamuscado.',
        ''
      ],
      receitaNumeroLike: '559',
      receitaNumeroDislike: '23',
      receitaValorAvaliacao: '4',
      receitaQtdeAvaliacoes: '983'
    },
    {
      receitaId: '9',
      receitaTitulo: 'Espaguete à Carbonara',
      receitaPostadaEm: '05/09/2018',
      receitaIdEspecialidade: '5',
      receitaImagem:
        'https://s2.glbimg.com/fkFkU2MIOher_0qhzAqTHGBeFhE=/0x0:507x338/984x0/smart/filters:strip_icc()/s2.glbimg.com/Kpgvs3M5-Ve_6wYzVDA10f4ixs0%3D/s.glbimg.com/et/gs/f/original/2016/11/18/carbonara_gshow.jpg',
      receitaIngredientes: [
        '200 g espaguete (ou outra massa longa de grano duro)',
        '½ xícara (chá) de bacon em cubos (75 g)',
        '¼ de xícara (chá) de vinho branco',
        '2 ovos',
        '2 gemas',
        '½ xícara (chá) de queijo parmesão ralado',
        'sal e pimenta-do-reino moída na hora a gosto',
        ''
      ],
      receitaModoPreparo: [
        'Leve uma panela média com 2 litros de água ao fogo alto.',
        'Quando ferver, adicione 1 colher (sopa) de sal, junte o macarrão e misture.',
        'Deixe cozinhar, pelo tempo indicado na embalagem ou até ficar al dente.',
        'Enquanto o macarrão cozinha, prepare os outros ingredientes.',
        'Numa tigela pequena quebre um ovo de cada vez e transfira para outra tigela (lembre-se: são 2 ovos inteiros e 2 gemas; você pode reservar as claras na geladeira por dois dias ou, melhor ainda, congelar para usar depois).',
        'Junte o queijo parmesão ralado e bata bem com um garfo para misturar.',
        'Leve uma frigideira grande ao fogo médio.',
        'Quando aquecer, doure o bacon por cerca de 5 minutos, mexendo de vez em quando.',
        'Diminua o fogo, regue o vinho branco, com cuidado para não espirrar, misture bem e desligue o fogo.',
        'Assim que o macarrão estiver cozido, reserve 1 xícara (chá) da água do cozimento.',
        'Agora você vai precisar fazer tudo bem rapidinho: escorra a água, transfira o macarrão para a frigideira com o bacon quente, junte os ovos e misture bem.',
        'A ideia é que o calor da massa cozinhe os ovos, formando um creme.',
        'Volte a frigideira ao fogo bem baixinho e vá adicionando a água do cozimento reservada aos poucos, mexendo com uma espátula por alguns minutinhos até formar um molho espesso.',
        'Cuidado para não cozinhar demais os ovos.',
        'Sirva a seguir com pimenta-do-reino a gosto.',
        ''
      ],
      receitaNumeroLike: '367',
      receitaNumeroDislike: '13',
      receitaValorAvaliacao: '5',
      receitaQtdeAvaliacoes: '357'
    },
    {
      receitaId: '10',
      receitaTitulo: 'Bruschetta de Tomate e Manjericão',
      receitaPostadaEm: '05/04/2015',
      receitaIdEspecialidade: '6',
      receitaImagem:
        'https://s2.glbimg.com/yepMp9sZUqaIq2g0sc_VDCe0he8=/0x0:1600x1067/984x0/smart/filters:strip_icc()/s.glbimg.com/po/rc/media/2013/11/15/15_54_41_185_bruschettas2.jpg',
      receitaIngredientes: [
        '1 dente de alho',
        '4 fatias de pão italiano',
        '2 tomates picados',
        'Sal e pimenta-do-reino a gosto',
        '1 colher (sopa) de folhas de manjericão picadas',
        '1 colher (sopa) de azeite',
        ''
      ],
      receitaModoPreparo: [
        'Corte o alho ao meio e esfregue-o em todas as fatias de pão italiano.',
        'Coloque-as numa pequena assadeira e leve ao forno pré-aquecido em temperatura média por 10 minutos, ou até as fatias dourarem levemente.',
        'Tempere o tomate com o sal e a pimenta.',
        'Ponha o manjericão, distribuindo-o entre as fatias quentes do pão.',
        'Regue com azeite e sirva.',
        ''
      ],
      receitaNumeroLike: '256',
      receitaNumeroDislike: '4',
      receitaValorAvaliacao: '3',
      receitaQtdeAvaliacoes: '124'
    },
    {
      receitaId: '11',
      receitaTitulo: 'Bolinho de Bacalhau',
      receitaPostadaEm: '15/04/2017',
      receitaIdEspecialidade: '6',
      receitaImagem:
        'https://vovopalmirinha.com.br/wp-content/uploads/2019/04/bolinho-de-bacalhau.jpg',
      receitaIngredientes: [
        '1kg de batata cozida',
        '1kg de bacalhau dessalgado e cozido',
        '1 xícara bem cheia de cheiro-verde picado',
        '1 ovo ligeiramente batido',
        'Óleo pra fritar',
        ''
      ],
      receitaModoPreparo: [
        'Em uma vasilha grande, coloque a batata passada pelo espremedor, o bacalhau desfiado o cheiro-verde e o ovo batido.',
        'Misture com uma colher e, a seguir, mexa com as mãos.',
        'Modele os bolinhos com a ajuda de duas colheres, passando a massa de uma pra a outra.',
        'Frite os bolinhos em óleo não muito quente.',
        'Escora em papel-toalha pra que fiquem sequinhos.',
        'Dica :',
        'Lave o bacalhau e deixe-o de molho em leite por 6 horas.',
        'Cozinhe as batatas na mesma água em que o bacalhau foi cozido.',
        ''
      ],
      receitaNumeroLike: '1.347',
      receitaNumeroDislike: '13',
      receitaValorAvaliacao: '4',
      receitaQtdeAvaliacoes: '1.132'
    },
    {
      receitaId: '12',
      receitaTitulo: 'Bolinho de Aipim com Carne Seca',
      receitaPostadaEm: '23/07/2019',
      receitaIdEspecialidade: '6',
      receitaImagem:
        'https://s2.glbimg.com/ApR-FA9HWmYFm-Vg8O7vhbL0iCY=/0x0:630x385/984x0/smart/filters:strip_icc()/s.glbimg.com/po/rc/media/2014/06/04/10_40_59_391_bolinho_de_mandioca_com_carne_seca.jpg',
      receitaIngredientes: [
        'Massa',
        '150g de mandioca',
        '1 xícara (chá) de caldo de legumes',
        '4 colheres (sobremesa) de manteiga',
        '1 xícara (chá) de leite',
        '2 xícaras (chá) de farinha de trigo',
        'Sal a gosto',
        'Recheio',
        '2 colheres (sopa) de óleo',
        '1 cebola média cortada em meias-luas',
        '300g de carne-seca dessalgada e desfiada',
        '1 ½ colher (sopa) de cheiro-verde picado',
        'Sal a gosto',
        '1 ovo',
        '½ xícara (chá) de farinha de rosca',
        'Óleo para fritar',
        ''
      ],
      receitaModoPreparo: [
        'Massa',
        'cozinhe a mandioca no caldo.',
        'Bata a mandioca com metade da manteiga e um pouco do caldo no liquidificador.',
        'Leve o leite e o restante do caldo para ferver.',
        'Acrescente a farinha e o purê de mandioca de uma vez, e cozinhe, mexendo até o ponto de desgrudar da panela.',
        'Coloque o restante da manteiga e mexa até incorporar bem.',
        'Deixe a massa esfriar, coberta com um pano úmido.',
        'Recheio',
        'Aqueça o óleo e doure a cebola.',
        'Acrescente a carne-seca e doure por 3 minutos.',
        'Coloque o cheiro-verde e corrija o sal.',
        'Montagem',
        'Abra na mão o equivalente a 1 colher (sobremesa) de massa.',
        'Coloque 1 colher (chá) de carne-seca e feche no formato de uma bola.',
        'Repita a operação até acabar a massa.',
        'Passe os bolinhos no ovo batido e depois na farinha de rosca.',
        'Frite em óleo quente.',
        ''
      ],
      receitaNumeroLike: '736',
      receitaNumeroDislike: '4',
      receitaValorAvaliacao: '3',
      receitaQtdeAvaliacoes: '378'
    }
  ];

  const resultEspecialidade = [
    {
      id: '1',
      descricao: 'Sobremesas'
    },
    {
      id: '2',
      descricao: 'Culinária Portuguesa'
    },
    {
      id: '3',
      descricao: 'Carnes'
    },
    {
      id: '4',
      descricao: 'Culinária Brasileira'
    },
    {
      id: '5',
      descricao: 'Massas'
    },
    {
      id: '6',
      descricao: 'Petiscos'
    },
    {
      id: '7',
      descricao: 'Aves'
    }
  ];

  await saveFile(receitasDB, resultReceitas);
  await saveFile(especialidadesDB, resultEspecialidade);
};

adicionarDados();
